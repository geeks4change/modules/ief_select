# IEF Select

A temporary [hack](https://www.drupal.org/project/inline_entity_form/issues/2683125#comment-12552019) to display a select list instead of autocomplete in Inline Entity Forms.
